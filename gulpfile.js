var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function() {
    return gulp.src('./frontend/src/style/*.scss')
      .pipe(sass()) // Using gulp-sass
      .pipe(gulp.dest('./frontend/public'))
  });

gulp.task('watch', function() {
    gulp.watch('./frontend/src/style/*.scss', gulp.series('sass')); 
  })