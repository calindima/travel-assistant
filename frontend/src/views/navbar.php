<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
  <a class="navbar-brand" href="index.php">Travel Assistant</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
    <div class="navbar-nav">
    <?php if( !isset($_SESSION['user'])): ?>
      <a class="nav-item nav-link mx-2" href="signupProvider.php">Host an experience</a>
      <a class="nav-item nav-link mx-2" href="signupUser.php">Sign up</a>
      <a class="nav-item nav-link mx-2" href="loginForm.php">Login</a>
      <?php endif; ?>
      <?php if( isset($_SESSION['user'])): ?>
      <a class="nav-item nav-link mx-2" href="dashboard.php">Dashboard</a>
      <a class="nav-item nav-link mx-2" href="logout.php">Logout</a>
      <?php endif; ?>
    </div>
  </div>
</nav>