<?php 
session_start();
if(isset($_SESSION['user'])) header('Location: dashboard.php');

?>
<?php require dirname(__DIR__) . '/src/views/header.php'; ?>
<?php require dirname(__DIR__) . '/src/views/navbar.php'; ?>


<form class="m-5" id="findExp">
    <h2>Find experiences from your favourite city!</h2>
    <div class="form-group">
        <label for="selectLocation">Select a location:</label>
        <select class="form-control" name="location" id="selectLocation">
            <!-- locations will be dynamically inserted here -->
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Find experiences</button>
</form>


<div class="container hide">
    <h2 class="text-center">Search results</h2>
    <div class="row d-flex justify-content-center" id="results">

    </div>
</div>


<script>
    fetch("../../backend/apis/api-location/api-showAllLocations.php")
        .then(res => res.json())
        .then(data => {
            data.forEach(item => {
                const el = document.createElement('option');
                const parent = document.querySelector('#selectLocation');

                el.textContent = `${item.cCity}, ${item.cCountry}`;

                parent.append(el);
            });
        });

    document.querySelector('#findExp').addEventListener('submit', e => {
        e.preventDefault();
        const formData = new FormData(e.target);

        fetch('../../backend/apis/api-experience/api-searchExperience.php', {
                method: 'POST',
                body: formData
            })
            .then((response) => response.json())
            .then((data) => {
                // console.log('Success:', data);
                
                const parent = document.querySelector('#results');
                parent.innerHTML = '';
                parent.parentElement.classList.remove('hide');

                data.forEach(item => {
                    const el = document.createElement('div');
                    el.classList.add('card');
                    el.classList.add('col-5');
                    el.classList.add('m-3');
                    el.setAttribute('id', `xpId-${item.nExperienceID}`);
                    // el.classList.add('text-center');

                    el.innerHTML = `
                    <div class="card-body d-flex flex-column justify-content-between">
                        <h5 class="card-title">${item.cName}</h5>
                        <p class="card-text">${item.txtDescription}</p>
                        <div class="row mb-3 font-weight-bold">
                            <div class="col">
                                <p class="card-text">Location: ${item.cCity}, ${item.cCountry}</p>
                            </div>
                            <div class="col">
                                <p class="card-text">Price: ${item.nPrice} DKK</p>
                            </div>
                        </div>
                        <a href="loginForm.php" class="btn btn-primary">Book now</a>
                    </div>
                                `;

                    parent.append(el);

                    el.querySelector('a.btn').addEventListener('click', e => {
                        console.log(e.target.parentElement.parentElement);
                    });
            });
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    });
</script>

<?php require dirname(__DIR__) . '/src/views/footer.php'; ?>