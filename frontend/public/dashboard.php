<?php

session_start();
if (!isset($_SESSION['user'])) {
    if (!isset($_SESSION['provider'])) {
        $_SESSION['user'] = $_SESSION['traveler'];
    }
    if (!isset($_SESSION['traveler'])) {
        $_SESSION['user'] = $_SESSION['provider'];
    }
}

if (isset($_SESSION['user'])) {
    unset($_SESSION['provider']);
    unset($_SESSION['traveler']);
} else {
    header('Location: index.php');
}

?>

<?php require dirname(__DIR__) . '/src/views/header.php'; ?>
<?php require dirname(__DIR__) . '/src/views/navbar.php'; ?>
<div class="container">
    <form class="m-5" id="formEdit">
        <h2>Edit your profile:</h2>
        <div class="form-group row">
            <div class="col">
                <input type="text" class="form-control" placeholder="First name" name="firstName" id="firstName" value="<?php echo $_SESSION['user']['cFirstName'] ?>">
            </div>
            <div class="col">
                <input type="text" class="form-control" placeholder="Last name" name="lastName" id="lastName" value="<?php echo $_SESSION['user']['cLastName'] ?>">
            </div>
            <div class="col">
                <input type="email" class="form-control" placeholder="Email address" name="email" id="email" value="<?php echo $_SESSION['user']['cEmail'] ?>">
            </div>
        </div>
        <div class="form-group row">
            <div class="col">
                <input type="password" class="form-control" placeholder="Password" name="passcode" id="passcode" value="<?php echo $_SESSION['user']['cPassword'] ?>">
            </div>
            <div class="col">
                <input type="number" class="form-control" placeholder="Phone number" name="phoneNumber" id="phoneNumber" value="<?php echo $_SESSION['user']['cPhoneNumber'] ?>">
            </div>
            <?php if (isset($_SESSION['user']['txtFullAddress'])) : ?>
                <div class="col">
                    <input type="text" class="form-control" placeholder="Full address" name="fullAddress" id="fullAddress" value="<?php echo $_SESSION['user']['txtFullAddress'] ?>">
                </div>
            <?php endif; ?>
        </div>

        <button type="submit" class="btn btn-primary">Edit</button>
        <button class="btn btn-danger">Cancel account</button>
    </form>

    <?php if (isset($_SESSION['user']['txtFullAddress'])) : ?>

        <form class="m-5" id="findExp">
            <h2>Find experiences from your favourite city!</h2>
            <div class="form-group">
                <label for="selectLocation">Select a location:</label>
                <select class="form-control w-50" name="location" id="selectLocation">
                    <!-- locations will be dynamically inserted here -->
                </select>
            </div>


            <button type="submit" class="btn btn-primary">Find experiences</button>
            <button class="btn btn-primary" id="btnShowAllExp">Show all experiences</button>
</div>
</form>
</div>
<div class="container hide">
    <h2 class="text-center">Search results</h2>
    <div class="row d-flex justify-content-center" id="results">

    </div>
</div>

<?php else : ?>
    <div class="container d-flex justify-content-end">
    </div>

    <div class="container m-2">
            <h2 class="text-center">My experiences</h2>
        <div class="row d-flex justify-content-end align-items-center mx-5 my-2">
            <a href="createExp.php" class="btn btn-outline-dark m-3">Create new experience</a>
        </div>
        <div class="row d-flex justify-content-center" id="myExp">

        </div>
    </div>


<?php endif; ?>
<script>
    if (document.querySelector('#findExp')) {
        // Fetch locations to insert in dropdown
        fetch("../../backend/apis/api-location/api-showAllLocations.php")
            .then(res => res.json())
            .then(data => {
                data.forEach(item => {
                    const el = document.createElement('option');
                    const parent = document.querySelector('#selectLocation');

                    el.textContent = `${item.cCity}, ${item.cCountry}`;

                    parent.append(el);
                });
            });

        // Show filtered experiences
        document.querySelector('#findExp').addEventListener('submit', e => {
            e.preventDefault();
            const formData = new FormData(e.target);

            fetch('../../backend/apis/api-experience/api-searchExperience.php', {
                    method: 'POST',
                    body: formData
                })
                .then((response) => response.json())
                .then((data) => {
                    // console.log('Success:', data);

                    const parent = document.querySelector('#results');
                    parent.innerHTML = '';
                    parent.parentElement.classList.remove('hide');

                    data.forEach(item => {
                        const el = document.createElement('div');
                        el.classList.add('card');
                        el.classList.add('col-6');
                        el.setAttribute('id', `xpId-${item.nExperienceID}`);
                        // el.classList.add('text-center');

                        el.innerHTML = `
                    <div class="card-body d-flex flex-column justify-content-between">
                        <h5 class="card-title">${item.cName}</h5>
                        <p class="card-text">${item.txtDescription}</p>
                        <div class="row mb-3 font-weight-bold">
                            <div class="col">
                                <p class="card-text">Location: ${item.cCity}, ${item.cCountry}</p>
                            </div>
                            <div class="col">
                                <p class="card-text">Price: ${item.nPrice} DKK</p>
                            </div>
                        </div>
                    <a href="#" class="btn btn-primary">Book now</a>
                    </div>
                                `;

                        parent.append(el);

                        el.querySelector('a.btn').addEventListener('click', e => {
                            console.log(e.target.parentElement.parentElement);
                            e.preventDefault();

                   
                            const formBook = document.createElement('form');
                            formBook.setAttribute('action', 'bookExp.php');
                            formBook.setAttribute('method', 'post');
                            formBook.classList.add('hide');
                            const experienceIdInput = document.createElement('input');
                            experienceIdInput.setAttribute('type', 'number');
                            experienceIdInput.setAttribute('name','experienceId');
                            experienceIdInput.setAttribute('value', item.nExperienceID);
                            const experiencePriceInput = document.createElement('input');
                            experiencePriceInput.setAttribute('type', 'number');
                            experiencePriceInput.setAttribute('name','experiencePrice');
                            experiencePriceInput.setAttribute('value', item.nPrice);
                            formBook.append(experienceIdInput);
                            formBook.append(experiencePriceInput);
                            document.querySelector('body').append(formBook);
                            formBook.submit();

                        });
                    });
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
        });

        // Show all experiences
        document.querySelector('#btnShowAllExp').addEventListener('click', e => {
            e.preventDefault();

            fetch('../../backend/apis/api-experience/api-showAllExperiences.php')
                .then((response) => response.json())
                .then((data) => {
                    console.log('Success:', data);

                    const parent = document.querySelector('#results');
                    parent.innerHTML = '';
                    parent.parentElement.classList.remove('hide');

                    data.forEach(item => {
                        const el = document.createElement('div');
                        el.classList.add('card');
                        el.classList.add('col-5');
                        el.classList.add('m-3');
                        el.setAttribute('id', `xpId-${item.nExperienceID}`);
                        // el.classList.add('text-center');

                        el.innerHTML = `
                    <div class="card-body d-flex flex-column justify-content-between">
                        <h5 class="card-title">${item.cName}</h5>
                        <p class="card-text">${item.txtDescription}</p>
                        <div class="row mb-3 font-weight-bold">
                            <div class="col">
                                <p class="card-text">Location: ${item.cCity}, ${item.cCountry}</p>
                            </div>
                            <div class="col">
                                <p class="card-text">Price: ${item.nPrice} DKK</p>
                            </div>
                        </div>
                        <a href="#" class="btn btn-primary">Book now</a>
                    </div>
                                `;

                        parent.append(el);

                        el.querySelector('a.btn').addEventListener('click', e => {
                            console.log(e.target.parentElement.parentElement);
                            e.preventDefault();

                   
                            const formBook = document.createElement('form');
                            formBook.setAttribute('action', 'bookExp.php');
                            formBook.setAttribute('method', 'post');
                            formBook.classList.add('hide');
                            const experienceIdInput = document.createElement('input');
                            experienceIdInput.setAttribute('type', 'number');
                            experienceIdInput.setAttribute('name','experienceId');
                            experienceIdInput.setAttribute('value', item.nExperienceID);
                            const experiencePriceInput = document.createElement('input');
                            experiencePriceInput.setAttribute('type', 'number');
                            experiencePriceInput.setAttribute('name','experiencePrice');
                            experiencePriceInput.setAttribute('value', item.nPrice);
                            formBook.append(experienceIdInput);
                            formBook.append(experiencePriceInput);
                            document.querySelector('body').append(formBook);
                            formBook.submit();
                        });
                    });
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
        });
    }

    // Edit profile
    document.querySelector('#formEdit').addEventListener('submit', e => {
        e.preventDefault();
        const formData = new FormData(e.target);

        let url = '../../backend/apis/api-user/api-update.php';

        if (!formData.has('fullAddress')) url = '../../backend/apis/api-provider/api-update.php';

        console.log(url);

        const options = {
            method: 'post',
            body: formData
        };

        fetch(url, options)
            .then(res => res.json())
            .then(data => {
                console.log(data)
            })
            .catch(e => {
                console.error(e)
            });
    });
    if (!document.querySelector('#findExp')) {
    // Show experiences for the logged in provider
    fetch('../../backend/apis/api-experience/api-showExperienceByProvider.php')
        .then((response) => response.json())
        .then((data) => {
            console.log('Success:', data);

            const parent = document.querySelector('#myExp');

            data.forEach(item => {
                const el = document.createElement('div');
                el.classList.add('card');
                el.classList.add('col-5');
                el.classList.add('m-3');
                el.setAttribute('id', `xpId-${item.nExperienceID}`);
                // el.classList.add('text-center');

                el.innerHTML = `
                    <div class="card-body d-flex flex-column justify-content-between">
                        <h5 class="card-title">${item.cName}</h5>
                        <p class="card-text">${item.txtDescription}</p>
                        <div class="row mb-3 font-weight-bold">
                            <div class="col">
                                <p class="card-text">Location: ${item.cCity}, ${item.cCountry}</p>
                            </div>
                            <div class="col">
                                <p class="card-text">Price: ${item.nPrice} DKK</p>
                            </div>
                        </div>
                        <div class="row d-flex justify-content-end">
                            
                            <a href="#" class="btn btn-primary mx-2">Edit</a>
                            
                            <a href="#" class="btn btn-danger">Delete</a>
                          
                        </div>
                        </div>
                                `;

                parent.append(el);

                el.querySelector('a.btn-primary').addEventListener('click', e => {
                    e.preventDefault();

                   
                    const formEdit = document.createElement('form');
                    formEdit.setAttribute('action', 'editExp.php');
                    formEdit.setAttribute('method', 'post');
                    formEdit.classList.add('hide');
                    const experienceIdInput = document.createElement('input');
                    experienceIdInput.setAttribute('type', 'number');
                    experienceIdInput.setAttribute('name','experienceId');
                    experienceIdInput.setAttribute('value', item.nExperienceID);
                    const titleInput = document.createElement('input');
                    titleInput.setAttribute('type', 'text');
                    titleInput.setAttribute('name', 'title');
                    titleInput.setAttribute('value', item.cName);
                    const descriptionInput = document.createElement('input');
                    descriptionInput.setAttribute('type', 'text');
                    descriptionInput.setAttribute('name', 'description');
                    descriptionInput.setAttribute('value', item.txtDescription);
                    const priceInput = document.createElement('input');
                    priceInput.setAttribute('type', 'number');
                    priceInput.setAttribute('name', 'price');
                    priceInput.setAttribute('value', item.nPrice);
                    formEdit.append(titleInput);
                    formEdit.append(descriptionInput);
                    formEdit.append(priceInput);
                    formEdit.append(experienceIdInput);
                    document.querySelector('body').append(formEdit);
                    formEdit.submit();
                    
                });
                el.querySelector('a.btn-danger').addEventListener('click', e => {
                    e.preventDefault();
                    const cardElem = e.target.parentElement.parentElement.parentElement;
                    
                    const formData = new FormData();
                    formData.append('experienceId', item.nExperienceID);

                    fetch('../../backend/apis/api-experience/api-deleteExperience.php',{
                        method: 'post',
                        body: formData
                    })
                    .then(res => res.json())
                    .then(data => {
                        console.log(data);
                        cardElem.remove();
                    })
                    .catch(e => console.error(e))
                    
                });
            });
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }
</script>

<?php require dirname(__DIR__) . '/src/views/footer.php'; ?>