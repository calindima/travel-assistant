<?php require dirname(__DIR__) . '/src/views/header.php'; ?>
<?php require dirname(__DIR__) . '/src/views/navbar.php'; ?>
<div class="container">
    <h2 class="text-center mt-5">Host experiences</h2>
<form class="m-5" method="POST" action="../../backend/apis/api-provider/api-signup.php">
    <div class="form-group row">
        <div class="col">
            <input type="text" class="form-control" placeholder="First name" name="firstName" value="John">
        </div>
        <div class="col">
            <input type="text" class="form-control" placeholder="Last name" name="lastName" value="Doe">
        </div>
    </div>
    <div class="form-group row">
        <div class="col">
            <input type="email" class="form-control" placeholder="Email address" name="email" value="jdoe@test.com">
        </div>
    </div>
    <div class="form-group row">
        <div class="col">
            <input type="password" class="form-control" placeholder="Password" name="passcode" value="123456">
        </div>
    </div>
    <div class="form-group row">
        <div class="col">
            <input type="text" class="form-control" placeholder="Username" name="username" value="jdoeuser">
        </div>
    </div>
    <div class="form-group row">
        <div class="col">
            <input type="number" class="form-control" placeholder="Phone-no" name="phoneNumber" value="87654321">
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Sign up</button>
</form>
</div>
<?php require dirname(__DIR__) . '/src/views/footer.php'; ?>