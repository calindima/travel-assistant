<?php 
session_start();
if(isset($_SESSION['user'])) header('Location: dashboard.php');

?>


<?php require dirname(__DIR__) . '/src/views/header.php'; ?>
<?php require dirname(__DIR__) . '/src/views/navbar.php'; ?>
<div class="container">
    <h2 class="text-center mt-5">Login</h2>

<form class="m-5" id="formLogin">
    <div class="form-group row">
        <div class="col">
            <input type="email" class="form-control" placeholder="Email address" name="email" value="jdoe@gmail.com">
        </div>
    </div>
    <div class="form-group row">
        <div class="col">
            <input type="password" class="form-control" placeholder="Password" name="passcode" value="1234">
        </div>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="accountOptions" id="travelerRadio" value="traveler" checked>
        <label class="form-check-label" for="travelerRadio">Traveler</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="accountOptions" id="providerRadio" value="provider">
        <label class="form-check-label" for="providerRadio">Provider</label>
    </div>
    <button type="submit" class="btn btn-primary">Login</button>
</form>
    <p class="text-center">Don't have an account? Create one <a href="signupUser.php">here</a>!</p>
</div>
<script>
document.querySelector('#formLogin').addEventListener('submit', e => {
        e.preventDefault();
        const formData = new FormData(e.target);
        let url = '../../backend/apis/api-user/api-login.php';

        if(formData.get('accountOptions') == 'provider') {
            url = '../../backend/apis/api-provider/api-login.php';
        }
        
        console.log(url);

        e.target.setAttribute('method', 'POST');
        e.target.setAttribute('action', url);
        e.target.submit();
        
    });

</script>

<?php require dirname(__DIR__) . '/src/views/footer.php'; ?>

<?php require dirname(__DIR__) . '/src/views/footer.php'; ?>