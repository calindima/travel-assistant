<?php session_start(); ?>
<?php require dirname(__DIR__) . '/src/views/header.php'; ?>
<?php require dirname(__DIR__) . '/src/views/navbar.php'; ?>

<div class="container">
    <h2 class="text-center mt-5">Create a new experience</h2>

<form class="m-5" method="POST" action="../../backend/apis/api-experience/api-createExperience.php">
    <div class="form-group row">
        <div class="col">
            <input type="text" class="form-control" placeholder="Title" name="expName" value="Sail along the canals in Copenhagen">
        </div>
        <div class="col">
            <input type="number" class="form-control" placeholder="Price" name="price" value="199">
        </div>
    </div>
    <div class="form-group row">
        <div class="col">
            <input type="textarea" class="form-control" placeholder="Description" name="expDescription" value="Sail through the canals in Copenhagen and experience a wonderful and romantic night!">
        </div>
    </div>
    <div class="form-group">
        <label for="selectLocation">Select a location:</label>
        <select class="form-control" name="location" id="selectLocation">
           <!-- locations will be dynamically inserted here -->
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Create experience</button>
</form>
</div>
<script>
fetch("../../backend/apis/api-location/api-showAllLocations.php")
.then(res => res.json())
.then(data => {
    data.forEach( item => {
        const el = document.createElement('option');
        const parent = document.querySelector('#selectLocation');

        el.textContent = `${item.cCity}, ${item.cCountry}`;

        parent.append(el);
    });
});
</script>

<?php require dirname(__DIR__) . '/src/views/footer.php'; ?>