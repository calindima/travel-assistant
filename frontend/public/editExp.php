<?php
session_start();
if (!isset($_SESSION['user'])) header('Location: index.php');
?>


<?php require dirname(__DIR__) . '/src/views/header.php'; ?>
<?php require dirname(__DIR__) . '/src/views/navbar.php'; ?>
<div class="container">
    <h2 class="text-center mt-5">Edit experience</h2>
    <form class="my-4 mx-2" id="formEditExp">
        <input type="hidden" name="experienceId" id="experienceId" value="<?= $_POST['experienceId'] ?>">
        <div class="form-group row">
            <div class="col">
                <input type="text" class="form-control" placeholder="Title" name="title" id="title" value="<?= $_POST['title'] ?>">
            </div>
        </div>
        <div class="form-group row">
            <div class="col">
                <textarea class="form-control" placeholder="Description" name="description" id="description"><?= $_POST['description'] ?></textarea>
            </div>
        </div>
        <div class="form-group row">
            <div class="col">
                <input type="number" class="form-control" placeholder="Price" name="price" id="price" value="<?= $_POST['price'] ?>">
            </div>
        </div>
        <div class="d-flex justify-content-around">
                <button type="submit" class="btn btn-primary">Save changes</button>
                <a href="dashboard.php" class="btn btn-danger">Cancel</a>
                </div>
    </form>
</div>
<script>

    document.querySelector('#formEditExp').addEventListener('submit', e => {
        e.preventDefault();
        const formData = new FormData(e.target);

        fetch('../../backend/apis/api-experience/api-updateExperience.php', {
            method: 'post',
            body: formData
            })
            .then(res => res.text())
            .then(data => {
                console.log(data)
                window.location = 'dashboard.php'
            })
            .catch(e => {
                console.error(e)
            });
    });


</script>