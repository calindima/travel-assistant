<?php
session_start();
?>

<?php require dirname(__DIR__) . '/src/views/header.php'; ?>
<?php require dirname(__DIR__) . '/src/views/navbar.php'; ?>

<div class="container">
    <h2 class="text-center mt-5">Book experience</h2>

    <form class="m-5" method="POST" action="../../backend/apis/api-experience/api-bookExperience.php">
        <h5 class="my-3">Card details:</h5>
        <div class="form-group row">
            <div class="col">
                <label for="IBAN">IBAN:</label>
                <input type="text" class="form-control" placeholder="IBAN" name="IBAN" id="IBAN" value="DK3920006895285623">
            </div>
            <div class="col-2">
                <label for="CCV">CCV:</label>
                <input type="number" class="form-control" placeholder="CCV" name="CCV" id="CCV" value="123">
            </div>
            <div class="col-2">
                <label for="expirationMonth">Month:</label>
                <select class="form-control" name="expirationMonth" id="expirationMonth">
                    <!-- months will be dynamically inserted here -->
                    <?php for ($i = 1; $i < 13; $i++) : ?>
                        <option><?= $i ?></option>
                    <?php endfor; ?>
                </select>
            </div>
            <div class="col-2">
                <label for="expirationYear">Year:</label>
                <select class="form-control" name="expirationYear" id="expirationYear">
                    <!-- Years will be dynamically inserted here -->
                    <?php for ($i = 2020; $i < 2099; $i++) : ?>
                        <option><?= $i ?></option>
                    <?php endfor; ?>
                </select>
            </div>
        </div>
        <h5 class="my-3">Experience details:</h5>
        <div class="form-group row">
            <div class="col">
            <label for="startDate">Start date:</label>
            <input type="date" class="form-control" id="startDate" name="startDate" value="<?= date('Y-m-d'); ?>" min="<?= date('Y-m-d'); ?>" max="2021-12-31">
            </div>
            <div class="col">
            <label for="personsAttending">Travelers:</label>
                <select class="form-control" name="personsAttending" id="personsAttending">
                    <!-- persons will be dynamically inserted here -->
                    <?php for ($i = 1; $i < 11; $i++) : ?>
                        <option><?= $i ?></option>
                    <?php endfor; ?>
                </select>
            </div>
        </div>
        <input type="hidden" name="experienceId" value="<?= $_POST['experienceId']; ?>">
        <input type="hidden" name="experiencePrice" value="<?= $_POST['experiencePrice']; ?>">
        <button type="submit" class="btn btn-primary my-3">Confirm booking</button>
    </form>
</div>

<script></script>

<?php require dirname(__DIR__) . '/src/views/footer.php'; ?>