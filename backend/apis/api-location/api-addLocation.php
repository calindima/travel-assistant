<?php

// fetch db config and connect php to mysql db
require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

// php signup script
$sCity = $_POST['city'];
$sCountry = $_POST['country'];

// TO DO: validate data

// push new provider to db
$stmt = $pdo->prepare('INSERT INTO locations(cCity, cCountry) 
VALUES  (:city, :country)');

$stmt->execute([
    'city' => $sCity,
    'country' => $sCountry
]);

echo "Location created successfully.";

// close connection
$stmt = null;
$pdo = null;