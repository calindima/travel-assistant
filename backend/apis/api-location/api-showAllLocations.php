<?php

require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

$stmt = $pdo->query('SELECT * from locations');

$jLocations = $stmt->fetchAll();

echo json_encode($jLocations);

// close connection
$stmt = null;
$pdo = null;

