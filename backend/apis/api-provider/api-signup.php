<?php

// fetch db config and connect php to mysql db
require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

// php signup script
$sFirstName = $_POST['firstName'];
$sLastName = $_POST['lastName'];
$sEmail = $_POST['email'];
$sPassword = $_POST['passcode'];
$sUsername = $_POST['username'];
$sPhoneNumber = $_POST['phoneNumber'];


$aProvider = [
    'firstName' => $sFirstName,
    'lastName' => $sLastName,
    'email' => $sEmail,
    'passcode'=> $sPassword,
    'username' => $sUsername,
    'phoneNumber' => $sPhoneNumber
];

// echo json_encode($aProvider);

header('Location: ../../../frontend/public/accountCreated.php');
/*

// TO DO: validate data

// push new provider to db
$stmt = $pdo->prepare('INSERT INTO providers(cFirstName, cLastName, cEmail, cPassword, cUsername, cPhoneNumber) 
VALUES  (:firstName, :lastName, :email, :passcode, :username, :phoneNumber)');

$stmt->execute([
    'firstName' => $sFirstName,
    'lastName' => $sLastName,
    'email' => $sEmail,
    'passcode'=> $sPassword,
    'username' => $sUsername,
    'phoneNumber' => $sPhoneNumber
]);

echo "Provider created successfully.";

// close connection
$stmt = null;
$pdo = null;

*/