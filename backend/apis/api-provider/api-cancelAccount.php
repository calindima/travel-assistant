<?php

// fetch db config and connect php to mysql db
require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

// delete account
// $nProviderId = $_SESSION['provider']->nProviderID;
$nProviderId = '1';

$stmt = $pdo->prepare('UPDATE providers SET dCancelledAt = :dCurrentDate WHERE providers.nProviderID = :providerId');

$stmt->execute([
    'dCurrentDate' =>  date('Y-m-d H:i:s'),
    'providerId' => $nProviderId
]);

echo 'Account cancelled.';





// close connection
$stmt = null;
$pdo = null;

// // logout
// session_destroy();
// header("Location: index.php");