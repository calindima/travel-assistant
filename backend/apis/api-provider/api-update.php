<?php

// fetch db config and connect php to mysql db
require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

session_start();

// get data
// $sCPR = $_SESSION['provider']->cCPR;
$nProviderId = $_SESSION['user']['nProviderID'];
// $nProviderId = $_POST['providerId'];
$sFirstName = $_POST['firstName'];
$sLastName = $_POST['lastName'];
$sPassword = $_POST['passcode'];
$sPhoneNumber = $_POST['phoneNumber'];

echo json_encode([
    'providerId' => $nProviderId,
    'firstName' => $sFirstName,
    'lastName' => $sLastName,
    'passcode'=> $sPassword,
    'phoneNumber' => $sPhoneNumber
]);

/*

// update provider in db
$stmt = $pdo->prepare('UPDATE providers SET cFirstName = :firstName, cLastName = :lastName, cPassword = :passcode, cPhoneNumber = :phoneNumber WHERE nProviderID = :providerId');

$stmt->execute([
    'providerId' => $nProviderId,
    'firstName' => $sFirstName,
    'lastName' => $sLastName,
    'passcode'=> $sPassword,
    'phoneNumber' => $sPhoneNumber
]);

echo "Provider updated successfully.";

// close connection
$stmt = null;
$pdo = null;

*/