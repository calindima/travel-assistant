<?php

require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

session_start();
$nProviderId = $_SESSION['user']['nProviderID'];

$stmt = $pdo->prepare('SELECT * from experiences a INNER JOIN locations b ON a.nLocationID = b.nLocationID WHERE a.nProviderID = :providerId');

$stmt->execute([
    'providerId' => $nProviderId
]);

$jExperiences = json_encode($stmt->fetchAll());

echo $jExperiences;


// close connection
$stmt = null;
$pdo = null;

