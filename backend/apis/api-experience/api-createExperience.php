<?php

// fetch db config and connect php to mysql db
require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

// create experience
$sName = $_POST['expName'];
$sDescription = $_POST['expDescription'];
$nPrice = $_POST['price'];

$sLocation = $_POST['location'];
$aLocation = explode(', ', $sLocation);

$sCity = $aLocation[0];
$sCountry = $aLocation[1];
session_start();
$nProviderId = $_SESSION['user']['nProviderID'];
// $nProviderId = 1;
// TO DO: validate data

// push new experience to db
$stmt = $pdo->prepare('SELECT nLocationID FROM locations WHERE cCity = :sCity AND cCountry = :sCountry');

$stmt->execute([
    'sCity' => $sCity,
    'sCountry' => $sCountry
]);


$nLocationId = json_decode(json_encode($stmt->fetch()))->nLocationID;

$aExperience = [
    'experienceName' => $sName,
    'experienceDescription' => $sDescription,
    'price' => $nPrice,
    'providerId' => $nProviderId,
    'locationId'=> $nLocationId,
    'sCity' => $sCity,
    'sCountry' => $sCountry
];

// echo json_encode($aExperience);

$stmt = $pdo->prepare('INSERT INTO experiences(cName, txtDescription, nPrice, nLocationID, nProviderID) 
VALUES  (:experienceName, :experienceDescription, :price, :locationId, :providerId)');

$stmt->execute([
    'experienceName' => $sName,
    'experienceDescription' => $sDescription,
    'price' => $nPrice,
    'locationId'=> $nLocationId,
    'providerId' => $nProviderId
]);


// echo '{"status":"ok","message":"Experience created succesfully."}';

// close connection
$stmt = null;
$pdo = null;

header('Location: ../../../frontend/public/dashboard.php');

