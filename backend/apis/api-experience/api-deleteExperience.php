<?php

// fetch db config and connect php to mysql db
require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

// delete experience
$nExperienceID = $_POST['experienceId'];


$stmt = $pdo->prepare('DELETE FROM experiences WHERE nExperienceID = :experienceId');

$stmt->execute([
    'experienceId' => $nExperienceID
]);

echo '{"status":"ok","message":"Experience '.$nExperienceID.' deleted"}';

// close connection
$stmt = null;
$pdo = null;