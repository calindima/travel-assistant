<?php

// fetch db config and connect php to mysql db
require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

$sLocation = $_POST['location'];
$aLocation = explode(', ', $sLocation);

$sCity = $aLocation[0];
$sCountry = $aLocation[1];

$sSearchString = '%'.$sCity.'%';
$sSearchString1 = '%'.$sCountry.'%';

$stmt = $pdo->prepare('SELECT * FROM experiences a INNER JOIN locations b ON a.nLocationID = b.nLocationID WHERE a.nLocationID IN (SELECT nLocationID FROM locations WHERE cCity LIKE :sSearchString OR cCountry LIKE :sSearchString1)');

$stmt->execute([
    'sSearchString' => $sSearchString,
    'sSearchString1' => $sSearchString1
]);

echo json_encode($stmt->fetchAll());


// close connection
$stmt = null;
$pdo = null;