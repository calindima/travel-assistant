<?php

// fetch db config and connect php to mysql db
require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

// variables
$sIBAN = $_POST['IBAN'];
$nCCV = $_POST['CCV'];
$dExpirationDate = $_POST['expirationYear'].'-'.$_POST['expirationMonth'].'-01';
$dStartDate = $_POST['startDate'];
$nPaidAmount = $_POST['personsAttending']*$_POST['experiencePrice'];
$nExperienceId = $_POST['experienceId'];
session_start();
$nUserId = $_SESSION['user']['nUserID'];

// echo json_encode([
//     'iban' => $sIBAN,
//     'ccv' => $nCCV,
//     'expirationDate' => $dExpirationDate,
//     'userid' => $nUserId,
//     'experienceid' => $nExperienceId,
//     'paidAmount' => $nPaidAmount,
//     'startDate' => $dStartDate
//     ]);

$stmt = $pdo->prepare('CALL newPayment (:iban, :ccv, :expirationDate, :userid, :experienceid, :paidAmount, :startDate)');

$stmt->execute([
    'iban' => $sIBAN,
    'ccv' => $nCCV,
    'expirationDate' => $dExpirationDate,
    'userid' => $nUserId,
    'experienceid' => $nExperienceId,
    'paidAmount' => $nPaidAmount,
    'startDate' => $dStartDate
  ]);


// close connection
$stmt = null;
$pdo = null;

header('Location: ../../../frontend/public/dashboard.php');