<?php

// fetch db config and connect php to mysql db
require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

// update experience
$nExperienceID = $_POST['experienceId'];
$sName = $_POST['title'];
$sDescription = $_POST['description'];
$nPrice = $_POST['price'];


$stmt = $pdo->prepare('UPDATE experiences SET cName = :expName, txtDescription = :expDescription, nPrice = :price  WHERE nExperienceID = :experienceId');

$stmt->execute([
    'experienceId' => $nExperienceID,
    'expName' => $sName,
    'expDescription' => $sDescription,
    'price' => $nPrice
]);

echo 'Experience updated';

// close connection
$stmt = null;
$pdo = null;
