<?php

require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

$stmt = $pdo->query('SELECT * from experiences a INNER JOIN locations b ON a.nLocationID = b.nLocationID');

$jExperiences = json_encode($stmt->fetchAll());

echo $jExperiences;


// close connection
$stmt = null;
$pdo = null;

