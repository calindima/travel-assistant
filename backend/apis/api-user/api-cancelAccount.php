<?php

// fetch db config and connect php to mysql db
require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

// delete account
// $sUserCPR = $_SESSION['user']->sCPR;
$sUserCPR = '2709921235';

$stmt = $pdo->prepare('UPDATE users SET dCancelledAt = :dCurrentDate WHERE users.cCPR = :userCPR');

$stmt->execute([
    'dCurrentDate' =>  date('Y-m-d H:i:s'),
    'userCPR' => $sUserCPR
]);

echo 'Account cancelled.';





// close connection
$stmt = null;
$pdo = null;

// // logout
// session_destroy();
// header("Location: index.php");