<?php

// fetch db config and connect php to mysql db
require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

// php signin script
$sEmail = $_POST['email'];
$sPassword = $_POST['passcode'];

// $aAccount = [
//     'email' => $sEmail,
//     'passcode'=> $sPassword
// ];

// echo json_encode($aAccount);


// TO DO: validate data

// retrieve user from db
$stmt = $pdo->prepare('SELECT * from users WHERE cEmail = :email AND cPassword = :passcode');

$stmt->execute([
    'email' => $sEmail,
    'passcode'=> $sPassword
]);

$rows = $stmt->fetch();

// echo json_encode($rows);
session_start();
$_SESSION['traveler'] = $rows;

// close connection
$stmt = null;
$pdo = null;

header('Location: ../../../frontend/public/dashboard.php');
/**/