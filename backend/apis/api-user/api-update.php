<?php

// fetch db config and connect php to mysql db
require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

session_start();

// get data
$sCPR = $_SESSION['user']['cCPR'];
// $sCPR = $_POST['CPR'];
$sFirstName = $_POST['firstName'];
$sLastName = $_POST['lastName'];
$sPassword = $_POST['passcode'];
$sPhoneNumber = $_POST['phoneNumber'];
$sFullAddress = $_POST['fullAddress'];

// function updateData($pdo, $columnName, $fieldName, $newValue, $sCPR) {
//     $stmt = $pdo->prepare('UPDATE users SET '.$columnName.' = :'.$fieldName.' WHERE cCPR = :CPR');

//     $stmt->execute([
//         'CPR' => $_POST['CPR'],
//         $fieldName => $newValue
//     ]);  
// }
// echo json_encode([
//     'CPR' => $sCPR,
//     'firstName' => $sFirstName,
//     'lastName' => $sLastName,
//     'passcode'=> $sPassword,
//     'phoneNumber' => $sPhoneNumber,
//     'fullAddress' => $sFullAddress
// ]);

// update user in db
$stmt = $pdo->prepare('UPDATE users SET cFirstName = :firstName, cLastName = :lastName, cPassword = :passcode, cPhoneNumber = :phoneNumber, txtFullAddress = :fullAddress WHERE cCPR = :CPR');

$stmt->execute([
    'CPR' => $sCPR,
    'firstName' => $sFirstName,
    'lastName' => $sLastName,
    'passcode'=> $sPassword,
    'phoneNumber' => $sPhoneNumber,
    'fullAddress' => $sFullAddress
]);

echo '{"status":"ok","message":"User updated"}';

// close connection
$stmt = null;
$pdo = null;

