<?php

// fetch db config and connect php to mysql db
require_once("../../database/dbconfig.php");
require_once("../../database/pdomysqlconnect.php");

// php signup script
$sCPR = $_POST['CPR'];
$sFirstName = $_POST['firstName'];
$sLastName = $_POST['lastName'];
$sEmail = $_POST['email'];
$sPassword = $_POST['passcode'];
$sUsername = $_POST['username'];
$sPhoneNumber = $_POST['phoneNumber'];
$sFullAddress = $_POST['fullAddress'];


$aUser = [
    'CPR' => $sCPR,
    'firstName' => $sFirstName,
    'lastName' => $sLastName,
    'email' => $sEmail,
    'passcode'=> $sPassword,
    'username' => $sUsername,
    'phoneNumber' => $sPhoneNumber,
    'fullAddress' => $sFullAddress
];

// echo json_encode($aUser);

header('Location: ../../../frontend/public/accountCreated.php');

/*

// TO DO: validate data

// push new user to db
$stmt = $pdo->prepare('INSERT INTO users(cCPR, cFirstName, cLastName, cEmail, cPassword, cUsername, cPhoneNumber, txtFullAddress) 
VALUES  (:CPR, :firstName, :lastName, :email, :passcode, :username, :phoneNumber, :fullAddress)');

$stmt->execute([
    'CPR' => $sCPR,
    'firstName' => $sFirstName,
    'lastName' => $sLastName,
    'email' => $sEmail,
    'passcode'=> $sPassword,
    'username' => $sUsername,
    'phoneNumber' => $sPhoneNumber,
    'fullAddress' => $sFullAddress
]);

echo "User created successfully.";


// close connection
$stmt = null;
$pdo = null;


*/