<?php

require_once("./database/dbconfig.php");
require_once("./database/pdomysqlconnect.php");

// // test creating entity

// $stmt = $pdo->prepare('INSERT INTO users(cCPR, cFirstName, cLastName, cEmail, cPassword, cUsername, cPhoneNumber, txtFullAddress) 
// VALUES  (?,?,?,?,?,?,?,?)');

// $stmt->execute(['2709921235', 'Johnard', 'Doe', 'jarddoe@gmail.com', '1234', 'jarddoe', '12223344', 'Gardvej 123A']);

// $stmt = $pdo->query('SELECT * FROM users');

// $rows = $stmt->fetchAll();
  

// echo json_encode($rows);


// //  test php signup script
// $sCPR = '2709929999';
// $sFirstName = 'Ion';
// $sLastName = 'Popescu';
// $sEmail = 'ipopescu@test.com';
// $sPassword = 'ipopsrulz';
// $sUsername = 'ipop';
// $sPhoneNumber = '11111112';
// $sFullAddress = 'Parkvej 29B';

// // push new user to db
// $stmt = $pdo->prepare('INSERT INTO users(cCPR, cFirstName, cLastName, cEmail, cPassword, cUsername, cPhoneNumber, txtFullAddress) 
// VALUES  (:CPR, :firstName, :lastName, :email, :passcode, :username, :phoneNumber, :fullAddress)');

// $stmt->execute([
//     'CPR' => $sCPR,
//     'firstName' => $sFirstName,
//     'lastName' => $sLastName,
//     'email' => $sEmail,
//     'passcode'=> $sPassword,
//     'username' => $sUsername,
//     'phoneNumber' => $sPhoneNumber,
//     'fullAddress' => $sFullAddress
// ]);

// echo "User created successfully.";


// // php signin script
// // $sEmail = $_POST['email'];
// // $sPassword = $_POST['passcode'];
// $sEmail = 'jarddoe@gmail.com';
// $sPassword = '1234';

// // TO DO: validate data

// // retrieve user from db
// $stmt = $pdo->prepare('SELECT * from users WHERE cEmail = :email AND cPassword = :passcode');

// $stmt->execute([
//     'email' => $sEmail,
//     'passcode'=> $sPassword
// ]);

// $rows = $stmt->fetch();

// echo json_encode($rows);

// // test finding one location and returning id

// $sCity = 'Vienna';
// $sCountry ='Austria';

// $stmt = $pdo->prepare('SELECT nLocationID FROM locations WHERE cCity = :sCity AND cCountry = :sCountry');

// $stmt->execute([
//     'sCity' => $sCity,
//     'sCountry' => $sCountry
// ]);

// echo json_decode(json_encode($stmt->fetch()))->nLocationID;
 
// // test query experience on location
// $sSearchString = '%'.$_POST['searchString'].'%';

// $stmt = $pdo->prepare('SELECT * FROM experiences INNER JOIN locations ON experiences.nLocationID = locations.nLocationID WHERE experiences.nLocationID IN (SELECT nLocationID FROM locations WHERE cCity LIKE :sSearchString OR cCountry LIKE :sSearchString1)');

// $stmt->execute([
//     'sSearchString' => $sSearchString,
//     'sSearchString1' => $sSearchString
// ]);

// echo json_encode($stmt->fetchAll());

// // test create location
// $sCity = '';
// $sCountry = '';

// $stmt = $pdo->prepare('INSERT INTO locations(cCity, cCountry) VALUES(:sCity, :sCountry)');

// $stmt->execute([
//     'sCity' => $sCity,
//     'sCountry' => $sCountry
// ]);

// echo $stmt->fetch();

// // test create experiences

// $sName = 'Explore London\'s wonderful urban scene';
// $sDescription = 'London has an amazing urban culture. Join us and discover what it has to offer';
// $nPrice = '399';

// $sCity = 'London';
// $sCountry = 'United Kingdom';

// $nProviderId = '1';

// // push new experience to db
// $stmt = $pdo->prepare('SELECT nLocationID FROM locations WHERE cCity = :sCity AND cCountry = :sCountry');

// $stmt->execute([
//     'sCity' => $sCity,
//     'sCountry' => $sCountry
// ]);

// $nLocationId = json_decode(json_encode($stmt->fetch()))->nLocationID;

// $stmt = $pdo->prepare('INSERT INTO experiences(cName, txtDescription, nPrice, nLocationID, nProviderID) 
// VALUES  (:experienceName, :experienceDescription, :price, :locationId, :providerId)');

// $stmt->execute([
//     'experienceName' => $sName,
//     'experienceDescription' => $sDescription,
//     'price' => $nPrice,
//     'locationId'=> $nLocationId,
//     'providerId' => $nProviderId
// ]);

// echo 'Experience created';


// close connection
$stmt = null;
$pdo = null;