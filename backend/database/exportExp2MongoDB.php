<?php

require_once("dbconfig.php");
require_once("pdomysqlconnect.php");

$stmt = $pdo->query('SELECT a.nExperienceID, a.cName, a.txtDescription, a.nPrice, a.nProviderID, b.cCity, b.cCountry from experiences a INNER JOIN locations b ON a.nLocationID = b.nLocationID');

$jExperiences = json_encode($stmt->fetchAll());

echo $jExperiences;

//need to connect to MongoDB and insert $jExperiences into an experiences collection

// will use json to represent mongoDB, problems connecting
$fp = fopen('MongoDB.json', 'w');
fwrite($fp, $jExperiences);
fclose($fp);

// close connection
$stmt = null;
$pdo = null;

