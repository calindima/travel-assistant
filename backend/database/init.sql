-- Copy this line into mysql CLI to import database -> \. C:\xampp\htdocs\travel-assistant\backend\database\init.sql
-- database creation
DROP DATABASE travelAssistant;

CREATE DATABASE IF NOT EXISTS travelAssistant CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE travelAssistant;

-- ////  Users  ////

DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users (
    nUserID INT(11) AUTO_INCREMENT,
    cCPR CHAR(10),
    cFirstName VARCHAR(100) NOT NULL,
    cLastName VARCHAR(100) NOT NULL,
    cEmail VARCHAR(255) NOT NULL UNIQUE,
    cPassword VARCHAR(255) NOT NULL,
    cUsername VARCHAR(20) NOT NULL UNIQUE,
    cPhoneNumber CHAR(8) NOT NULL UNIQUE,
    txtFullAddress TEXT NOT NULL,
    nTotalPaid INT(11) NOT NULL DEFAULT 0,
    dJoinedAt DATETIME NOT NULL DEFAULT NOW(),
    dCancelledAt DATETIME,
    PRIMARY KEY(nUserID, cCPR)  
);


-- ////  Providers  ////

DROP TABLE IF EXISTS providers;
CREATE TABLE IF NOT EXISTS providers (
    nProviderID INT(11) AUTO_INCREMENT,
    cFirstName VARCHAR(100) NOT NULL,
    cLastName VARCHAR(100) NOT NULL,
    cEmail VARCHAR(255) NOT NULL UNIQUE,
    cPassword VARCHAR(255) NOT NULL,
    cUsername VARCHAR(20) NOT NULL UNIQUE,
    cPhoneNumber CHAR(8) NOT NULL UNIQUE,
    dJoinedAt DATETIME NOT NULL DEFAULT NOW(),
    dCancelledAt DATETIME,
    PRIMARY KEY(nProviderID)  
);



-- ////  Credit Cards  ////

DROP TABLE IF EXISTS creditCards;
CREATE TABLE IF NOT EXISTS creditCards (
    cIBAN CHAR(34),
    dExpirationDate DATE NOT NULL,
    nCCV INT(3) NOT NULL,
    nTotalPaid INT(11) NOT NULL DEFAULT 0,
    nUserID INT(11),
    PRIMARY KEY(cIBAN),
    CONSTRAINT fk_cardholder 
    FOREIGN KEY(nUserID)
    REFERENCES users(nUserID)
    ON UPDATE CASCADE
    ON DELETE CASCADE  
);

    

-- ////  Locations  ////

DROP TABLE IF EXISTS locations;
CREATE TABLE IF NOT EXISTS locations (
    nLocationID INT(11) AUTO_INCREMENT,
    cCity VARCHAR(50) NOT NULL,
    cCountry VARCHAR(50) NOT NULL,
    PRIMARY KEY(nLocationID)  
);

    

-- ////  Experiences  ////

DROP TABLE IF EXISTS experiences;
CREATE TABLE IF NOT EXISTS experiences (
    nExperienceID INT(11) AUTO_INCREMENT,
    cName VARCHAR(100) NOT NULL,
    txtDescription TEXT NOT NULL,
    nPrice INT(11) NOT NULL DEFAULT 0,
    nLocationID INT(11),
    nProviderID INT(11),
    PRIMARY KEY(nExperienceID),
    CONSTRAINT fk_location 
    FOREIGN KEY(nLocationID)
    REFERENCES locations(nLocationID)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
    CONSTRAINT fk_provider 
    FOREIGN KEY(nProviderID)
    REFERENCES providers(nProviderID)
    ON UPDATE CASCADE
    ON DELETE CASCADE  
);

-- ////  Payments  ////

DROP TABLE IF EXISTS payments;
CREATE TABLE IF NOT EXISTS payments (
    nPaymentID INT(11) AUTO_INCREMENT,
    cIBAN CHAR(34),
    nAmount INT(11) NOT NULL DEFAULT 0,
    dRegisteredAt DATETIME NOT NULL DEFAULT NOW(),
    PRIMARY KEY(nPaymentID),
    CONSTRAINT fk_cciban 
    FOREIGN KEY(cIBAN)
    REFERENCES creditCards(cIBAN)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);
    

-- ////  Reservations  ////

DROP TABLE IF EXISTS reservations;
CREATE TABLE IF NOT EXISTS reservations (
    nReservationID INT(11) AUTO_INCREMENT,
    nUserID INT(11),
    nExperienceID INT(11),
    cIBAN CHAR(34),
    dStartDate DATE NOT NULL,
    nTotalPrice INT(11) NOT NULL,
    dTimestamp DATETIME NOT NULL DEFAULT NOW(),
    PRIMARY KEY(nReservationID, nUserID, nExperienceID, cIBAN, dStartDate),
    CONSTRAINT fk_userid 
    FOREIGN KEY(nUserID)
    REFERENCES users(nUserID)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
    CONSTRAINT fk_experience 
    FOREIGN KEY(nExperienceID)
    REFERENCES experiences(nExperienceID)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
    CONSTRAINT fk_iban 
    FOREIGN KEY(cIBAN)
    REFERENCES creditCards(cIBAN)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

DROP TABLE IF EXISTS auditTable;
CREATE TABLE IF NOT EXISTS auditTable (
    nAuditID INT(11) AUTO_INCREMENT,
    cUser VARCHAR(255),
    cTableName VARCHAR(255),
    cFieldName VARCHAR(255),
    cStatementType VARCHAR(255),
    cOldValue VARCHAR(255),
    cNewValue VARCHAR(255),
    dTimestamp DATETIME NOT NULL DEFAULT NOW(),
    PRIMARY KEY (nAuditID)
);

-- Stored Procedures

DELIMITER $$
 
CREATE PROCEDURE newPayment (iban CHAR(34), ccv INT(3), expirationDate DATE, userid INT(11), experienceid INT(11), paidAmount INT(11), startDate DATE)
BEGIN
   
    START TRANSACTION;

    IF (SELECT EXISTS(SELECT 1 FROM creditCards WHERE cIBAN = iban)) THEN
        UPDATE creditCards 
        SET nTotalPaid = nTotalPaid + paidAmount
        WHERE cIBAN = iban;
    ELSE
        INSERT INTO creditCards (cIBAN, dExpirationDate, nCCV, nTotalPaid, nUserID) 
        VALUES (iban, expirationDate, ccv, paidAmount, userid);
    END IF;
    
    INSERT INTO payments (nAmount, cIBAN) VALUES (paidAmount, iban);

    UPDATE users
    SET nTotalPaid = nTotalPaid + paidAmount
    WHERE nUserID = userid;

    INSERT INTO reservations (nUserID, nExperienceID, cIBAN, dStartDate, nTotalPrice) 
    VALUES (userid, experienceid, iban, startDate, paidAmount);

    COMMIT;
END$$

CREATE PROCEDURE auditThis (user VARCHAR(255), tableName VARCHAR(255), fieldName VARCHAR(255), stmtType VARCHAR(255), oldValue VARCHAR(255), newValue VARCHAR(255))
BEGIN

INSERT INTO auditTable(cUser, cTableName, cFieldName, cStatementType, cOldValue, cNewValue)
VALUES (user, tableName, fieldName, stmtType, oldValue, newValue);

END$$
DELIMITER ;

-- Triggers
DELIMITER $$
 
CREATE TRIGGER audit_users_insert
    BEFORE INSERT
    ON users FOR EACH ROW
BEGIN
    CALL auditThis(USER(), 'users', 'cCPR', 'insert', '', NEW.cCPR);
    CALL auditThis(USER(), 'users', 'cFirstName', 'insert', '', NEW.cFirstName);
    CALL auditThis(USER(), 'users', 'cLastName', 'insert', '', NEW.cLastName);
    CALL auditThis(USER(), 'users', 'cEmail', 'insert', '', NEW.cEmail);
    CALL auditThis(USER(), 'users', 'cUsername', 'insert', '', NEW.cUsername);
    CALL auditThis(USER(), 'users', 'cPhoneNumber', 'insert', '', NEW.cPhoneNumber);
END$$ 
 
CREATE TRIGGER audit_creditCards_insert
    BEFORE INSERT
    ON creditCards FOR EACH ROW
BEGIN
    CALL auditThis(USER(), 'creditCards', 'cIBAN', 'insert', '', NEW.cIBAN);
    CALL auditThis(USER(), 'creditCards', 'dExpirationDate', 'insert', '', NEW.dExpirationDate);
    CALL auditThis(USER(), 'creditCards', 'nCCV', 'insert', '', NEW.nCCV);
    CALL auditThis(USER(), 'creditCards', 'nUserID', 'insert', '', NEW.nUserID);    
END$$
 
CREATE TRIGGER audit_payments_insert
    BEFORE INSERT
    ON payments FOR EACH ROW
BEGIN
    CALL auditThis(USER(), 'payments', 'cIBAN', 'insert', '', NEW.cIBAN);
    CALL auditThis(USER(), 'payments', 'nAmount', 'insert', '', NEW.nAmount);    
END$$  
 
CREATE TRIGGER audit_users_update
    BEFORE UPDATE
    ON users FOR EACH ROW
BEGIN
    IF OLD.cFirstName != NEW.cFirstName THEN 
        CALL auditThis(USER(), 'users', 'cFirstName', 'update', OLD.cFirstName, NEW.cFirstName);
    END IF;
    IF OLD.cLastName != NEW.cLastName THEN 
        CALL auditThis(USER(), 'users', 'cLastName', 'update', OLD.cLastName, NEW.cLastName);
    END IF;
    IF OLD.cPassword != NEW.cPassword THEN 
        CALL auditThis(USER(), 'users', 'cPassword', 'update', OLD.cPassword, NEW.cPassword);
    END IF;
    IF OLD.cPhoneNumber != NEW.cPhoneNumber THEN 
        CALL auditThis(USER(), 'users', 'cPhoneNumber', 'update', OLD.cPhoneNumber, NEW.cPhoneNumber);
    END IF;
    IF OLD.txtFullAddress != NEW.txtFullAddress THEN 
        CALL auditThis(USER(), 'users', 'txtFullAddress', 'update', OLD.txtFullAddress, NEW.txtFullAddress);
    END IF;
    
END$$    
 
CREATE TRIGGER audit_creditCards_update
    BEFORE UPDATE
    ON creditCards FOR EACH ROW
BEGIN
    IF OLD.nTotalPaid != NEW.nTotalPaid THEN 
        CALL auditThis(USER(), 'creditCards', 'nTotalPaid', 'update', OLD.nTotalPaid, NEW.nTotalPaid);
    END IF;
END$$    
 
 
CREATE TRIGGER audit_payments_update
    BEFORE UPDATE
    ON payments FOR EACH ROW
BEGIN
    IF OLD.nAmount != NEW.nAmount THEN 
        CALL auditThis(USER(), 'payments', 'nAmount', 'update', OLD.nAmount, NEW.nAmount);
    END IF;
END$$    
 
CREATE TRIGGER audit_users_delete
    BEFORE DELETE
    ON users FOR EACH ROW
BEGIN
    CALL auditThis(USER(), 'users', 'cCPR', 'delete', OLD.cCPR, '');
    CALL auditThis(USER(), 'users', 'cFirstName', 'delete', OLD.cFirstName, '');
    CALL auditThis(USER(), 'users', 'cLastName', 'delete', OLD.cLastName, '');
    CALL auditThis(USER(), 'users', 'cEmail', 'delete', OLD.cEmail, '');
    CALL auditThis(USER(), 'users', 'cUsername', 'delete', OLD.cUsername, '');
    CALL auditThis(USER(), 'users', 'cPhoneNumber', 'delete', OLD.cPhoneNumber, '');
    CALL auditThis(USER(), 'users', 'txtFullAddress', 'delete', OLD.txtFullAddress, '');
    CALL auditThis(USER(), 'users', 'nTotalPaid', 'delete', OLD.nTotalPaid, '');
END$$    
 
CREATE TRIGGER audit_creditCards_delete
    BEFORE DELETE
    ON creditCards FOR EACH ROW
BEGIN
    CALL auditThis(USER(), 'creditCards', 'cIBAN', 'delete', OLD.cIBAN, '');
    CALL auditThis(USER(), 'creditCards', 'dExpirationDate', 'delete', OLD.dExpirationDate, '');
    CALL auditThis(USER(), 'creditCards', 'nCCV', 'delete', OLD.nCCV, '');
    CALL auditThis(USER(), 'creditCards', 'nTotalPaid', 'delete', OLD.nTotalPaid, '');
    CALL auditThis(USER(), 'creditCards', 'nUserID', 'delete', OLD.nUserID, '');
END$$
 
CREATE TRIGGER audit_payments_delete
    BEFORE DELETE
    ON payments FOR EACH ROW
BEGIN
        CALL auditThis(USER(), 'payments', 'nPaymentID', 'delete', OLD.nPaymentID, '');
        CALL auditThis(USER(), 'payments', 'cIBAN', 'delete', OLD.cIBAN, '');
        CALL auditThis(USER(), 'payments', 'nAmount', 'delete', OLD.nAmount, '');
END$$
 
 
DELIMITER ;


-- Insert dummy data


INSERT INTO users(cCPR, cFirstName, cLastName, cEmail, cPassword, cUsername, cPhoneNumber, txtFullAddress) 
VALUES  ('2709921234', 'John', 'Doe', 'jdoe@gmail.com', '1234', 'jdoe', '11223344', 'Gardvej 123A'),
        ('2709921000', 'Jane', 'Doe', 'janedoe@gmail.com', '1234', 'janedoe', '11223346', 'Gardvej 123A'),
        ('2709921001', 'Johnny', 'Doe', 'jydoe@gmail.com', '1234', 'jydoe', '11223347', 'Gardvej 123B'),
        ('2709921002', 'Janey', 'Doe', 'janeydoe@gmail.com', '1234', 'janeydoe', '11223348', 'Gardvej 13A'),
        ('2709921003', 'Ion', 'Popescu', 'ipop@gmail.com', '1234', 'ipopescu', '11223349', 'Valbygade 987');
    
INSERT INTO providers(cFirstName, cLastName, cEmail, cPassword, cUsername, cPhoneNumber) 
VALUES  ('John', 'Doe', 'jdoe@gmail.com', '1234', 'jdoe', '11223344'),
        ('Viorel', 'Geambasu', 'vigi@gmail.com', '1234', 'vgeam', '11223345'),
        ('John', 'Smith', 'jsmith@gmail.com', '1234', 'jsmith', '11223346'),
        ('Carl', 'Johnson', 'cjay@gmail.com', '1234', 'cjay', '11223347'),
        ('Alissa', 'Beauvais', 'alibeau@gmail.com', '1234', 'alib', '11223348'),
        ('Joanna', 'Ricci', 'jricci@gmail.com', '1234', 'riccijay', '11223349'),
        ('Laquesha', 'Darc', 'laqd@gmail.com', '1234', 'laqd', '11223340');

INSERT INTO locations (cCity,cCountry)
VALUES  ('London', 'UK'), 
        ('Vienna', 'Austria'),
        ('Copenhagen', 'Denmark'),
        ('Alicante', 'Spain'),
        ('Barcelona', 'Spain'),
        ('Moscow', 'Russia'),
        ('Bucharest', 'Romania'),
        ('Milan', 'Italy'),
        ('Phuket', 'Thailand'),
        ('Oslo', 'Norway');

        
INSERT INTO experiences (cName, txtDescription, nPrice, nLocationID, nProviderID)
VALUES  ('Minimalistic Copenhagen', 'A tour of the wonderfully minimalistic city of Copenhagen where you can experience the Nordic life', 399, 3, 1),    
        ('London walking tour', 'Experience the great British city of London on foot. Cheers!', 250, 1, 1),
        ('Experience 3', 'This is experience 3 for testing purposes', 1337, 2, 2),
        ('Experience 4', 'This is experience 4 for testing purposes', 1337, 4, 3),
        ('Experience 5', 'This is experience 5 for testing purposes', 1337, 5, 4),
        ('Experience 6', 'This is experience 6 for testing purposes', 1337, 6, 5),
        ('Experience 7', 'This is experience 7 for testing purposes', 1337, 7, 6),
        ('Experience 8', 'This is experience 8 for testing purposes', 1337, 8, 5),
        ('Experience 9', 'This is experience 9 for testing purposes', 1337, 8, 6),
        ('Experience 10', 'This is experience 10 for testing purposes', 1337, 9, 2);

                
CALL newPayment ('DK00000000000000000000000000000001','100','2020-12-01', 1, 1, 399, '2020-03-12');
CALL newPayment ('DK00000000000000000000000000000002','101','2021-01-01', 1, 5, 1337, '2020-05-22');
CALL newPayment ('DK00000000000000000000000000000003','102','2020-09-01', 2, 1, 1197, '2020-03-13');
CALL newPayment ('DK00000000000000000000000000000004','103','2020-11-01', 2, 2, 250, '2020-05-12');
CALL newPayment ('DK00000000000000000000000000000005','104','2021-12-01', 3, 6, 1337, '2020-06-12');
CALL newPayment ('DK00000000000000000000000000000006','105','2020-12-01', 3, 7, 1337, '2020-07-12');
CALL newPayment ('DK00000000000000000000000000000007','106','2020-10-01', 4, 8, 1337, '2020-08-12');
CALL newPayment ('DK00000000000000000000000000000008','107','2021-03-01', 4, 10, 1337, '2020-03-25');
CALL newPayment ('DK00000000000000000000000000000009','108','2022-06-01', 5, 1, 399, '2020-10-12');
CALL newPayment ('DK00000000000000000000000000000010','109','2021-07-01', 5, 2, 250, '2020-03-20');